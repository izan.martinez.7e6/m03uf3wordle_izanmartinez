﻿/*
 * AUTHOR: Izan Martinez Tomas
 * DATE: 28/02/2023
 * DESCRIPTION: Farem un joc de tipus wordle el qual consta de adivinar un aparaula aleatoria, que ens dona el joc.
 */
using System;
using System.IO;
using System.Collections.Generic;

namespace ProjecteWoordel
{
    public class ProjecteWordle
    {
        static void Main(string[] args)
        {
            string opcioIdioma = "";
            do
            {
                string directori = "../../../../../FitxersWordle/";
                string path = directori + "ListaDeIdiomas.txt";


                Console.Clear();
                string line;
                Console.WriteLine("\nLanguage List:\n");

                StreamReader sr = new StreamReader(path);

                line = sr.ReadLine();

                while (line != null)
                {

                    Console.WriteLine(line);

                    line = sr.ReadLine();
                }

                sr.Close();

                Console.WriteLine("\nChoose Language or show puntuations:\n");
                opcioIdioma = Console.ReadLine();

                switch (opcioIdioma)
                {
                    case "cat":
                        path = directori + "lenguajes/cat.txt";
                        JuegoWordle(path);
                        break;
                    case "es":
                        path = directori + "lenguajes/es.txt";
                        JuegoWordle(path);
                        break;
                    case "en":
                        path = directori + "lenguajes/en.txt";
                        JuegoWordle(path);
                        break;
                    case "pt":
                        MostrarPuntuaciones(directori);
                        break;
                    case "end":
                        Console.WriteLine("Ending game");
                        break;  
                    default:
                        Console.WriteLine("Invalid Option...Error!");
                        break;
                }
            } while (opcioIdioma.ToUpper() != "END");

        }
        /// <summary>
        /// En esta funcion mostraremos las puntuaciones con los nombres registrados al finalizar la partida en caso de que la palabra haya sido adivinada.
        /// </summary>
        /// <param name="directori"></param>
        public static void MostrarPuntuaciones(string directori)
        {
            string path = directori + "Puntuacion.txt";
            string line;
            StreamReader sr = new StreamReader(path);
            Console.Clear();
            while ((line = sr.ReadLine()) != null)
            {
                Console.WriteLine("\n");
                Console.WriteLine(line);
            }

            sr.Close();
            Console.ReadKey();
        }
        /// <summary>
        /// Esta funcion contiene las funcionalidades primarias de nuestro wordle.
        /// </summary>
        /// <param name="path"></param>
        public static void JuegoWordle(string path)
        {
            string paraula = ParaulaAleatoria(path);

            Console.Clear();

            MostrarParaulas(path);

            string tornarAJugar = "";
            do
            {
                Jugar(paraula);
                Console.WriteLine("Try again? Si/No");
                tornarAJugar = Console.ReadLine();
                Console.Clear();
            } while (tornarAJugar.ToUpper() == "SI");
        }
        /// <summary>
        /// Mostraremos las intrucciones de nuestro wordle.
        /// </summary>
        /// <param name="path"></param>
        public static void MostrarParaulas(string path)
        {
            string line;
            StreamReader sr = new StreamReader(path);
            int contador = 0;

            while ((line = sr.ReadLine()) != null)
            {
                if (contador < 9)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(line);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(line);
                }
                Console.ResetColor();
                contador++;
            }

            sr.Close();
        }
        /// <summary>
        /// Esta es la funcion con la que compararemos las palabras que nosotros damos con la que se genera aleatoriamente.
        /// </summary>
        /// <param name="paraula"></param>
        public static void Jugar(string paraula)
        {
            string paraulaJoc = paraula;
            string paraulaDonada = "";
            bool dentro = false;

            for (int i = 0, z = 4; i < 6; i++, z--)
            {
                Console.WriteLine($"5 letter word {z + 1} attempts");
                paraulaDonada = Console.ReadLine();
                while (paraulaDonada.Length != 5)
                {
                    Console.WriteLine("The word doesn't have the correct length, type one that has 5 letters:");
                    paraulaDonada = Console.ReadLine();
                }
                for (int x = 0; x < 5; x++)
                {
                    if (paraulaDonada[x] == paraulaJoc[x])
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(paraulaDonada[x]);
                        Console.ResetColor();
                    }
                    else if (paraulaDonada[x] != paraulaJoc[x])
                    {
                        for (int esta = 0; esta < 5; esta++)
                        {
                            if (paraulaDonada[x] == paraulaJoc[esta])
                            {
                                Console.BackgroundColor = ConsoleColor.Yellow;
                                Console.ForegroundColor = ConsoleColor.Black;
                                Console.Write(paraulaDonada[x]);
                                Console.ResetColor();
                                dentro = true;
                            }
                        }
                        if (dentro == false)
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.Write(paraulaDonada[x]);
                            Console.ResetColor();
                        }
                        dentro = false;
                    }
                    else Console.Write(paraulaDonada[x]);
                }
                Console.WriteLine();
                if (paraulaDonada == paraulaJoc)
                {
                    Console.WriteLine("CONGRATULATIONS");
                    Console.WriteLine("What is your name?");
                    string nombre = Console.ReadLine();
                    int puntuacion = i;
                    string rutaArchivo = "../../../../../FitxersWordle/Puntuacion.txt";

                    using (StreamWriter writer = File.AppendText(rutaArchivo))
                    {
                        writer.WriteLine($"{nombre} \t Attemps: {puntuacion} \t Word: {paraulaJoc}");
                    }
                    i = 7;  
                }
                Console.WriteLine();
            }
        }
        /// <summary>
        /// Funcion con la que generamos la palabra aleatoria.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ParaulaAleatoria(string path)
        {
            string paraula = "";
            string ruta = "../../../../../FitxersWordle/lenguajes/";
            
            string contenido = "";

            if (path == ruta + "cat.txt")
            {
                contenido = File.ReadAllText(ruta + "ParaulesCat.txt");
            }
            else if (path == ruta + "es.txt")
            {
                contenido = File.ReadAllText(ruta + "ParaulesEs.txt");
            }
            else if (path == ruta + "en.txt")
            {
                contenido = File.ReadAllText(ruta + "ParaulesEn.txt");
            }
            string[] palabrasSeparadas = contenido.Split(',');
            Random rnd = new Random();
            int indiceAleatorio = rnd.Next(0, palabrasSeparadas.Length);
            string paraulaMayus = palabrasSeparadas[indiceAleatorio].ToLower();
            string paraulaSinSpace = paraulaMayus.Replace(" ", "");
            paraula = paraulaSinSpace;

            return paraula;
        }
        public static int ComprobarPalabra(string paraula)
        {
            return paraula.Length;
        }
        public static bool ComprobarArray(string[] array) 
        {
            for(int i = 0; i < array.Length; i++)
            {
                if(int.TryParse(array[i],out int palabra))
                {
                    return false;
                }
            }
            return true;
        }

    }
}
