using NUnit.Framework;
using ProjecteWoordel;

namespace UnitTestWordle
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ComprobarLongitud()
        {
            Assert.AreEqual(5, ProjecteWoordel.ProjecteWordle.ComprobarPalabra("temas"));
        }
        [Test]
        public void ComprobarArray()
        {
            Assert.AreEqual(false, ProjecteWoordel.ProjecteWordle.ComprobarArray(new string[3] {"temas","cacas","43233"}));
        }
    }
}